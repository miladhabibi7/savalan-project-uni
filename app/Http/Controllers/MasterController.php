<?php

namespace App\Http\Controllers;

use App\Models\Master;
use App\Models\Student;
use App\Models\Course;
use Illuminate\Http\Request;

use App\Http\Requests\StoreMasterRequest;
use App\Http\Requests\UpdateMasterRequest;

class MasterController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {

        $masters = Master::orderBy('created_at', 'desc')->simplePaginate(15);
        return view('admin.content.master.index', compact('masters'));
    //    $master= Course::factory()->count(20)->create();
        // $master=factory(Master::class,20)->create();
        // // dd('hi');
        //
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        $courses = Course::all();
        
        return view('admin.content.master.create', compact('courses'));
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        // dd($request->all());
        $master=new Master() ;
        $master->first_name = $request->first_name;
        $master->last_name = $request->last_name;
        $master->save();
        return redirect()->route('admin.content.master.index')->with('swal-success', 'استاد  جدید  با موفقیت اضافه شد');
        
        //
    }

    /**
     * Display the specified resource.
     */
    public function show(Master $master)
    {
        return view('admin.content.master.show', compact('master'));
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Master $master)
    {
        return view('admin.content.master.edit', compact('master'));
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, Master $master)
    {

        $inputs = $request->all();
        
        

        $master->update($inputs);
        return redirect()->route('admin.content.master.index')->with('swal-success', 'استاد مورد نظر با موفقیت ویرایش شد');
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Master $master)
    {
        $result = $master->delete();
        return redirect()->route('admin.content.master.index')->with('swal-success', 'استاد موفقیت حذف شد');
        //
    }
}
