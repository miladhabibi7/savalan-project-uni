<?php

namespace App\Http\Controllers;

use App\Models\Selected_course;
use App\Http\Requests\StoreSelected_courseRequest;
use App\Http\Requests\UpdateSelected_courseRequest;

class SelectedCourseController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(StoreSelected_courseRequest $request)
    {
        //
    }

    /**
     * Display the specified resource.
     */
    public function show(Selected_course $selected_course)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Selected_course $selected_course)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(UpdateSelected_courseRequest $request, Selected_course $selected_course)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Selected_course $selected_course)
    {
        //
    }
}
