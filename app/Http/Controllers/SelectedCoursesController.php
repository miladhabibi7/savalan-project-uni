<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreSelected_coursesRequest;
use App\Http\Requests\UpdateSelected_coursesRequest;
use App\Models\Selected_course;
use App\Models\Available_course;
use App\Models\Student;
use App\Models\Master;
use App\Models\Course;
use App\Models\Term;
use Illuminate\Http\Request;


class SelectedCoursesController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {

        


        $selected_courses = Selected_course::orderBy('created_at', 'desc')->simplePaginate(15);
        return view('admin.content.selected_course.index', compact('selected_courses'));
        
        //
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {

        $courses = Course::all();
        $masters = Master::all();
        $students=Student::all();
        $terms=Term::all();
        

        return view('admin.content.selected_course.create', compact('courses','masters','students','terms'));
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        // dd($request->all());

        $inputs = $request->all();
        $available_course = new Available_course();
        $available_course->master_id = $request->master_id; 
        $available_course->course_id = $request->course_id; 
        $available_course->term_id = $request->term_id;
        $available_course->save(); 
        

        $selected_course = new Selected_course();
        $selected_course->available_id = $available_course->id;
        $selected_course->student_id = $request->student_id;
        $selected_course->save();
       


        return redirect()->route('admin.content.selected_course.index')->with('swal-success', 'انتخاب واحد جدید  با موفقیت انجام شد');
        //
    }

    /**
     * Display the specified resource.
     */
    public function show(Selected_course $selected_course)
    {
        return view('admin.content.selected_course.show', compact('selected_course'));
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Selected_course $selected_course)
    {

        $courses = Course::all();
        $masters = Master::all();
        $terms = Term::all();
        $students = Student::all();
        
        return view('admin.content.selected_course.edit', compact('selected_course', 'courses','masters','terms','students'));
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, Selected_course $selected_course)
    {
        //  dd( $selected_course->available_course);
        //  dd( $request->all() );
        $selected_course->student_id =$request->student_id ;
        $selected_course->available_course->master_id=$request->master_id ;
        $selected_course->available_course->course_id=$request->course_id ;
        $selected_course->available_course->term_id=$request->term_id ;
       $selected_course->available_course->save();
        $selected_course->save();


        return redirect()->route('admin.content.selected_course.index')->with('swal-success', 'انتخاب واحد جدید  با موفقیت به روز شد');




        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Selected_course $selected_course)
    {

        $result = $selected_course->delete();
        return redirect()->route('admin.content.selected_course.index')->with('swal-success', 'درس ارائه شده  شما با موفقیت حذف شد');
        //
    }
}
