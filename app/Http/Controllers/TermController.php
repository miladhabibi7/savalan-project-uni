<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreTermRequest;
use App\Http\Requests\UpdateTermRequest;
use App\Models\Term;
use Illuminate\Http\Request;

class TermController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {

        $terms = Term::orderBy('created_at', 'desc')->simplePaginate(15);
        return view('admin.content.term.index', compact('terms'));
        //
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {

        return view('admin.content.term.create');
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {

        $inputs = $request->all();
        $term = Term::create($inputs);
        return redirect()->route('admin.content.term.index')->with('swal-success', 'ترم مورد نظر با موفقیت اضافه شد');
        //
    }

    /**
     * Display the specified resource.
     */
    public function show(Term $term)
    {

        return view('admin.content.term.show', compact('term'));
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Term $term)
    {

        return view('admin.content.term.edit', compact('term'));
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, Term $term)
    {

        $inputs = $request->all();
        
        

        $term->update($inputs);
        return redirect()->route('admin.content.term.index')->with('swal-success', 'ترم مورد نظر با موفقیت ویرایش شد');
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Term $term)
    {


        $result = $term->delete();
        return redirect()->route('admin.content.term.index')->with('swal-success', 'ترم مورد نظر با موفقیت حذف شد');
        //
    }
}
