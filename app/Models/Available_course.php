<?php

namespace App\Models;
use App\Models\Course;
use App\Models\Student;
use App\Models\Term;
use App\Models\Selected_course;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Available_course extends Model
{
    use HasFactory;
    // public function selected_course(){
    //     return $this->hasmany(Selected_course::class,'available_id');
    // }
    public function course()
    {
        return $this->belongsTo(Course::class,'course_id');
    }
    public function master()
    {
        return $this->belongsTo(Master::class,'master_id');
    }

    public function students(){
        return $this->belongsToMany(Student::class,'selected_courses','available_id','student_id');
    }

    public function selected_courses()
    {
        return $this->hasMany(Selected_course::class,'available_id');
    }

    public function term()
    {
        return $this->belongsTo(Term::class,'term_id');
    }

    protected $fillable = ['master_id', 'course_id','term_id'];
}
