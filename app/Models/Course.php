<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Master;
use App\Models\Available_course;

class Course extends Model
{
    use HasFactory;
    public function masters(){
        return $this->belongsToMany(Master::class,'available_courses');
    }


    public function available_courses()
    {
        return $this->hasMany(Available_course::class,'course_id');
    }
    protected $fillable = ['name'];
}
