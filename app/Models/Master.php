<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Course;

class Master extends Model
{
    use HasFactory;
    public function courses(){
        return $this->belongsToMany(Course::class,'available_courses');
    }

    public function available_courses()
    {
        return $this->hasMany(Available_course::class,'master_id');
    }
    protected $fillable = ['first_name', 'last_name'];
}
