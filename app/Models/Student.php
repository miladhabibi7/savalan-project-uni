<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Available_course;
use App\Models\Student;
use App\Models\Selected_course;


class Student extends Model
{
    use HasFactory;
    // public function selected_course(){
    //     return $this->hasmany(Selected_course::class);
    // }

    public function selected_courses()
    {
        return $this->hasMany(Selected_course::class,'student_id');
    }

    public function availableCourses(){
        return $this->belongsToMany(Available_course::class,'selected_courses','available_id','student_id');
    }

    protected $fillable = ['first_name', 'last_name'];
  
}
