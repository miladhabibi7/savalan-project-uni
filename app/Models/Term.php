<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Term extends Model
{
    use HasFactory;


    public function available_courses()
    {
        return $this->hasMany(Available_course::class,'term_id');
    }
    protected $fillable = [ 'name'];
}
