<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use App\Models\Master;
use App\Models\Course;
/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Available_course>
 */
class Available_courseFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [

            'master_id'=> function(){
                return Master::factory()->create()->id;
            },

            'course_id'=> function(){
                return Course::factory()->create()->id;
            }
            //
        ];
    }
}
