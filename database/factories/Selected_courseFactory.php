<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use App\Models\Available_course;
use App\Models\Student;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Selected_courses>
 */
class Selected_courseFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            'available_id'=> function(){
                return Available_course::factory()->create()->id;
            },

            'student_id'=> function(){
                return Student::factory()->create()->id;
            }
            //
        ];
    }
}
