@extends('admin.layouts.master')

@section('head-tag')
<title>پیج ساز</title>
@endsection

@section('content')





<nav aria-label="breadcrumb">
    <ol class="breadcrumb">
      <li class="breadcrumb-item font-size-12"> <a href="#">خانه</a></li>
      <li class="breadcrumb-item font-size-12"> <a href="#">بخش فروش</a></li>
      <li class="breadcrumb-item font-size-12"> <a href="#">پیج ساز</a></li>
      <li class="breadcrumb-item font-size-12 active" aria-current="page">  انتخاب واحد</li>
    </ol>
  </nav>


  <section class="row">
    <section class="col-12">
        <section class="main-body-container">
            <section class="main-body-container-header">
                <h5>
                  انتخاب واحد 
                </h5>
            </section>

            <section class="d-flex justify-content-between align-items-center mt-4 mb-3 border-bottom pb-2">
                <a href="{{ route('admin.content.selected_course.index') }}" class="btn btn-info btn-sm">بازگشت</a>
            </section>

            <section>
                <form action="{{ route('admin.content.selected_course.store') }}" method="post" id="form">
                    @csrf
                    <section class="row">

                        <section class="col-12 col-md-6">
                            <div class="form-group">
                                <label for="">دروس </label>
                                <select name="course_id" id="" class="form-control form-control-sm">
                                    <option value="">درس را انتخاب کنید</option>

                                    @foreach ($courses as $course)
                                    <option value="{{ $course->id }} @if(old('course_id') == $course->id) selected @endif" >{{ $course->name }}</option>
                                    @endforeach
                                    
                                   
                                   </select>
                            </div>
                            @error('category_id')
                            <span class="alert_required bg-danger text-white p-1 rounded" role="alert">
                                <strong>
                                    {{ $message }}
                                </strong>
                            </span>
                        @enderror
                        </section>




                        <section class="col-12 col-md-6">
                            <div class="form-group">
                                <label for="">اساتید </label>
                                <select name="master_id" id="" class="form-control form-control-sm">
                                    <option value="">استاد را انتخاب کنید</option>

                                    @foreach ($masters as $master)
                                    <option value="{{ $master->id }} @if(old('master_id') == $master->id) selected @endif" >{{( $master->last_name) }}</option>
                                    @endforeach
                                   
                                   </select>
                            </div>
                            @error('category_id')
                            <span class="alert_required bg-danger text-white p-1 rounded" role="alert">
                                <strong>
                                    {{ $message }}
                                </strong>
                            </span>
                        @enderror
                        </section>





                        <section class="col-12 col-md-6">
                            <div class="form-group">
                                <label for="">دانشجویان </label>
                                <select name="student_id" id="" class="form-control form-control-sm">
                                    <option value="">دانشجو را انتخاب کنید</option>

                                    @foreach ($students as $student)
                                    <option value="{{ $student->id }} @if(old('student_id') == $student->id) selected @endif" >{{( $student->last_name) }}</option>
                                    @endforeach
                                   
                                   </select>
                            </div>
                            @error('category_id')
                            <span class="alert_required bg-danger text-white p-1 rounded" role="alert">
                                <strong>
                                    {{ $message }}
                                </strong>
                            </span>
                        @enderror
                        </section>


                       



                        <section class="col-12">
                            <button class="btn btn-primary btn-sm">ثبت</button>
                        </section>
                    </section>
                </form>
            </section>

        </section>
    </section>
</section>

@endsection
@section('script')

    <script src="{{ asset('admin-assets/ckeditor/ckeditor.js') }}"></script>
    <script>
        CKEDITOR.replace('body');
    </script>

<script>
    $(document).ready(function () {
        var tags_input = $('#tags');
        var select_tags = $('#select_tags');
        var default_tags = tags_input.val();
        var default_data = null;

        if(tags_input.val() !== null && tags_input.val().length > 0)
        {
            default_data = default_tags.split(',');
        }

        select_tags.select2({
            placeholder : 'لطفا تگ های خود را وارد نمایید',
            tags: true,
            data: default_data
        });
        select_tags.children('option').attr('selected', true).trigger('change');


        $('#form').submit(function ( event ){
            if(select_tags.val() !== null && select_tags.val().length > 0){
                var selectedSource = select_tags.val().join(',');
                tags_input.val(selectedSource)
            }
        })
    })
</script>

@endsection
