<aside id="sidebar" class="sidebar">
    <section class="sidebar-container">
        <section class="sidebar-wrapper">

            <!-- <a href="" class="sidebar-link" target="_blank">
                <i class="fas fa-shopping-cart"></i>
                <span>فروشگاه</span>
            </a> -->

            <hr>


            <a href="{{ route('admin.home') }}" class="sidebar-link">
                <i class="fas fa-home"></i>
                <span>خانه</span>
            </a>

            <!-- <section class="sidebar-part-title">بخش فروش</section>

            <section class="sidebar-group-link">
                <section class="sidebar-dropdown-toggle">
                    <i class="fas fa-chart-bar icon"></i>
                    <span>ویترین</span>
                    <i class="fas fa-angle-left angle"></i>
                </section>
                <section class="sidebar-dropdown">
                    <a href="">دسته بندی</a>
                    <a href="">فرم کالا</a>
                    <a href="">برندها</a>
                    <a href="">کالاها</a>
                    <a href="">انبار</a>
                    <a href="">نظرات</a>
                </section>
            </section>

            <section class="sidebar-group-link">
                <section class="sidebar-dropdown-toggle">
                    <i class="fas fa-chart-bar icon"></i>
                    <span>سفارشات</span>
                    <i class="fas fa-angle-left angle"></i>
                </section>
                <section class="sidebar-dropdown">
                    <a href=""> جدید</a>
                    <a href="">در حال ارسال</a>
                    <a href="">پرداخت نشده</a>
                    <a href="">باطل شده</a>
                    <a href="">مرجوعی</a>
                    <a href="">تمام سفارشات</a>
                </section>
            </section>

            <section class="sidebar-group-link">
                <section class="sidebar-dropdown-toggle">
                    <i class="fas fa-chart-bar icon"></i>
                    <span>پرداخت ها</span>
                    <i class="fas fa-angle-left angle"></i>
                </section>
                <section class="sidebar-dropdown">
                    <a href="">تمام پرداخت ها</a>
                    <a href="">پرداخت های آنلاین</a>
                    <a href="">پرداخت های آفلاین</a>
                    <a href="">پرداخت در محل</a>
                </section>
            </section>

            <section class="sidebar-group-link">
                <section class="sidebar-dropdown-toggle">
                    <i class="fas fa-chart-bar icon"></i>
                    <span>تخفیف ها</span>
                    <i class="fas fa-angle-left angle"></i>
                </section>
                <section class="sidebar-dropdown">
                    <a href="">کپن تخفیف</a>
                    <a href="">تخفیف عمومی</a>
                    <a href="">فروش شگفت انگیز</a>
                </section>
            </section>

            <a href="" class="sidebar-link">
                <i class="fas fa-bars"></i>
                <span>روش های ارسال</span>
            </a> -->



            <section class="sidebar-part-title">بخش محتوی</section>
            {{-- @role('operator') --}}
            <a href="{{route('admin.content.master.index')}}" class="sidebar-link">
                <i class="fas fa-bars"></i>
                <span>اساتید</span>
            </a>
            {{-- @endrole --}}

            <a href="{{route('admin.content.student.index')}}" class="sidebar-link">
                <i class="fas fa-bars"></i>
                <span>دانشجویان</span>
            </a>
            <a href="{{route('admin.content.course.index')}}" class="sidebar-link">
                <i class="fas fa-bars"></i>
                <span>درس ها</span>
            </a>

            <a href="{{route('admin.content.available_course.index')}}" class="sidebar-link">
                <i class="fas fa-bars"></i>
                <span>دروس ارائه شده</span>
            </a>

            <a href="{{route('admin.content.selected_course.index')}}" class="sidebar-link">
                <i class="fas fa-bars"></i>
                <span>دروس انتخاب شده</span>
            </a>


            <a href="{{route('admin.content.term.index')}}" class="sidebar-link">
                <i class="fas fa-bars"></i>
                <span>ترم ها  </span>
            </a>
            <!-- <a href="" class="sidebar-link">
                <i class="fas fa-bars"></i>
                <span>منو</span>
            </a>
            <a href="" class="sidebar-link">
                <i class="fas fa-bars"></i>
                <span>سوالات متداول</span>
            </a>
            <a href="" class="sidebar-link">
                <i class="fas fa-bars"></i>
                <span>پیج ساز</span>
            </a>
            <a href="" class="sidebar-link">
                <i class="fas fa-bars"></i>
                <span>بنر ها</span>
            </a> -->


<!-- 
            <section class="sidebar-part-title">بخش کاربران</section>
            <a href="" class="sidebar-link">
                <i class="fas fa-bars"></i>
                <span>کاربران ادمین</span>
            </a>
            <a href="" class="sidebar-link">
                <i class="fas fa-bars"></i>
                <span>مشتریان</span>
            </a>
            <section class="sidebar-group-link">
                <section class="sidebar-dropdown-toggle">
                    <i class="fas fa-chart-bar icon"></i>
                    <span>سطوح دسترسی</span>
                    <i class="fas fa-angle-left angle"></i>
                </section>
                <section class="sidebar-dropdown">
                    <a href="">مدیریت نقش ها</a>
                    <a href="">مدیریت دسترسی ها</a>
                    <a href="">فروش شگفت انگیز</a>
                </section>
            </section> -->


<!-- 
            <section class="sidebar-part-title">تیکت ها</section>
            <a href="" class="sidebar-link">
                <i class="fas fa-bars"></i>
                <span> دسته بندی تیکت ها </span>
            </a>
            <a href="" class="sidebar-link">
                <i class="fas fa-bars"></i>
                <span> اولویت تیکت ها </span>
            </a>
            <a href="" class="sidebar-link">
                <i class="fas fa-bars"></i>
                <span> ادمین تیکت ها </span>
            </a>
            <a href="" class="sidebar-link">
                <i class="fas fa-bars"></i>
                <span>تیکت های جدید</span>
            </a>
            <a href="" class="sidebar-link">
                <i class="fas fa-bars"></i>
                <span>تیکت های باز</span>
            </a>
            <a href="" class="sidebar-link">
                <i class="fas fa-bars"></i>
                <span>تیکت های بسته</span>
            </a>

            <a href="" class="sidebar-link">
                <i class="fas fa-bars"></i>
                <span>همه ی تیکت ها</span>
            </a> -->



            <section class="sidebar-part-title">اطلاع رسانی</section>
            <!-- <a href="" class="sidebar-link">
                <i class="fas fa-bars"></i>
                <span>اعلامیه ایمیلی</span>
            </a>
            <a href="" class="sidebar-link">
                <i class="fas fa-bars"></i>
                <span>اعلامیه پیامکی</span>
            </a> -->



            <section class="sidebar-part-title">تنظیمات</section>
            <a href="" class="sidebar-link">
                <i class="fas fa-bars"></i>
                <span>تنظیمات</span>
            </a>

        </section>
    </section>
</aside>
