<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\MasterController;
use App\Http\Controllers\CourseController;
use App\Http\Controllers\TermController;
use App\Http\Controllers\StudentController;
use App\Http\Controllers\SelectedCoursesController;
use App\Http\Controllers\AdminDashboardController;
use App\Http\Controllers\AvailableCourseController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::prefix('admin')->namespace('Admin')->group(function () {

    Route::get('/', [AdminDashboardController::class, 'index'])->name('admin.home');

      //student
      Route::prefix('students')->group(function () {
        Route::get('/', [StudentController::class, 'index'])->name('admin.content.student.index');
        Route::get('/create', [StudentController::class, 'create'])->name('admin.content.student.create');
        Route::post('/store', [StudentController::class, 'store'])->name('admin.content.student.store');
        Route::get('/edit/{student}', [StudentController::class, 'edit'])->name('admin.content.student.edit');
        Route::put('/update/{student}', [StudentController::class, 'update'])->name('admin.content.student.update');
        Route::delete('/destroy/{student}', [StudentController::class, 'destroy'])->name('admin.content.student.destroy');
        Route::get('/show/{student}', [StudentController::class, 'show'])->name('admin.content.student.show');
    });


    //master
    Route::prefix('masters')->group(function () {
        Route::get('/', [MasterController::class, 'index'])->name('admin.content.master.index');
        Route::get('/create', [MasterController::class, 'create'])->name('admin.content.master.create');
        Route::post('/store', [MasterController::class, 'store'])->name('admin.content.master.store');
        Route::get('/edit/{master}', [MasterController::class, 'edit'])->name('admin.content.master.edit');
        Route::put('/update/{master}', [MasterController::class, 'update'])->name('admin.content.master.update');
        Route::delete('/destroy/{master}', [MasterController::class, 'destroy'])->name('admin.content.master.destroy');
        Route::get('/show/{master}', [MasterController::class, 'show'])->name('admin.content.master.show');
    });



       //course
       Route::prefix('courses')->group(function () {
        Route::get('/', [CourseController::class, 'index'])->name('admin.content.course.index');
        Route::get('/create', [CourseController::class, 'create'])->name('admin.content.course.create');
        Route::post('/store', [CourseController::class, 'store'])->name('admin.content.course.store');
        Route::get('/edit/{course}', [CourseController::class, 'edit'])->name('admin.content.course.edit');
        Route::put('/update/{course}', [CourseController::class, 'update'])->name('admin.content.course.update');
        Route::delete('/destroy/{course}', [CourseController::class, 'destroy'])->name('admin.content.course.destroy');
        Route::get('/show/{course}', [CourseController::class, 'show'])->name('admin.content.course.show');
    });

        //available_course
        Route::prefix('available_course')->group(function () {
            Route::get('/', [AvailableCourseController::class, 'index'])->name('admin.content.available_course.index');
            Route::get('/create', [AvailableCourseController::class, 'create'])->name('admin.content.available_course.create');
            Route::post('/store', [AvailableCourseController::class, 'store'])->name('admin.content.available_course.store');
            Route::get('/edit/{available_course}', [AvailableCourseController::class, 'edit'])->name('admin.content.available_course.edit');
            Route::put('/update/{available_course}', [AvailableCourseController::class, 'update'])->name('admin.content.available_course.update');
            Route::delete('/destroy/{available_course}', [AvailableCourseController::class, 'destroy'])->name('admin.content.available_course.destroy');
            Route::get('/show/{available_course}', [AvailableCourseController::class, 'show'])->name('admin.content.available_course.show');
        });


         //selected_course
         Route::prefix('selected_course')->group(function () {
            Route::get('/', [SelectedCoursesController::class, 'index'])->name('admin.content.selected_course.index');
            Route::get('/create', [SelectedCoursesController::class, 'create'])->name('admin.content.selected_course.create');
            Route::post('/store', [SelectedCoursesController::class, 'store'])->name('admin.content.selected_course.store');
            Route::get('/edit/{selected_course}', [SelectedCoursesController::class, 'edit'])->name('admin.content.selected_course.edit');
            Route::put('/update/{selected_course}', [SelectedCoursesController::class, 'update'])->name('admin.content.selected_course.update');
            Route::delete('/destroy/{selected_course}', [SelectedCoursesController::class, 'destroy'])->name('admin.content.selected_course.destroy');
            Route::get('/show/{selected_course}', [SelectedCoursesController::class, 'show'])->name('admin.content.selected_course.show');
        });


            //term
       Route::prefix('term')->group(function () {
        Route::get('/', [TermController::class, 'index'])->name('admin.content.term.index');
        Route::get('/create', [TermController::class, 'create'])->name('admin.content.term.create');
        Route::post('/store', [TermController::class, 'store'])->name('admin.content.term.store');
        Route::get('/edit/{term}', [TermController::class, 'edit'])->name('admin.content.term.edit');
        Route::put('/update/{term}', [TermController::class, 'update'])->name('admin.content.term.update');
        Route::delete('/destroy/{term}', [TermController::class, 'destroy'])->name('admin.content.term.destroy');
        Route::get('/show/{term}', [TermController::class, 'show'])->name('admin.content.term.show');
    });


});

Route::get('/fact',  [MasterController::class, 'index']);
Route::get('/course',  [CourseController::class, 'index']);
